# DiOpNuCa

This project was developed to calculate numerically partial
differential derivatived using smooth particle hydrodynamics.
DiOpNuCa means differential Operator Numerical Calculation.

# Execution

Before running the program, you need to create a file (**derConst.txt**)
that should contain the following configuration information:

```bash
rNeigh = <float>
coefEps = <float>
Np = <Int>
```

Here, the first line specifies the search radius for finding the nearest particles,
and the second line specifies the coefficient for determining the parameter $\epsilon$
of the weighting function $w(x)$. The parameter (**Np**) in the last line is the number
of particles in the studied region.

To run the program, you should write the following command:

```bash
python3 diopnuca.py
```

# Output

After execution, you will receive a file (**derErrorFile.txt**) in the following format:

```go
x y ddX ddY
<number> <number> <number> <number>
<number> <number> <number> <number>
........ ........ ........ ........
<number> <number> <number> <number>
L2 error for XX-derivative: <number>
L2 error for YY-derivative: <number>
```

First, the table will be printed, which will contain the particle coordinates and
numerically calculated second-order partial derivatives for the Laplace operator.
At the end of the file, information about the error components of the Laplace operator
will be written on two lines.
