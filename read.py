
# read derivative constants from file
def readDerivativeConstants(derConstFileName):

    # open file
    file = open(derConstFileName, "r")

    # read and parse the first row
    row = file.readline()
    substrings = row.split(" ")
    rNeigh = float(substrings[2])

    # read and parse the second row
    row = file.readline()
    substrings = row.split(" ")
    coefEps = float(substrings[2])

    # read and parse the third row
    row = file.readline()
    substrings = row.split(" ")
    Np = int(substrings[2])

    # close file
    file.close()
    # return found coefficients
    return Np, rNeigh, coefEps
