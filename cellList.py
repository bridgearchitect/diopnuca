# include modules
import numpy as np
from math import trunc

# definition array of indeces for cells
class ArrayCellIndex:
    # constructor
    def __init__(self):
        # definition of indeces
        self.indeces = []

# definition of cell structure
class CellList:
    # constructor
    def __init__(self, x1, y1, x2, y2, Nx, Ny):
        # definitiona of fields
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        self.Nx = Nx
        self.Ny = Ny
        # definition of cell size
        self.ax = (x2 - x1) / Nx
        self.by = (y2 - y1) / Ny
        # fill array of cells using array of particles
        self.cells = [[ArrayCellIndex() for i in range(Nx)] for j in range(Ny)]

# function to fill cell list using array of points
def fillList(cellList, arrayPart):

    # find begin and end of region for cell list
    x1 = cellList.x1
    x2 = cellList.x2
    y1 = cellList.y1
    y2 = cellList.y2
    # find size of cell
    ax = cellList.ax
    by = cellList.by

    # go through all array of particles
    for i in range(len(arrayPart.particles)):
        # fetch current particle
        particle = arrayPart.particles[i]
        # determine particle coordinates
        xp = particle.x
        yp = particle.y
        # check whether point is essential inside
        if (x1 <= xp) and (xp <= x2) and (y1 <= yp) and (yp <= y2):
            # determine indeces for particle
            il = trunc((xp - x1) / ax)
            jl = trunc((yp - y1) / by)
            # add index of particle inside cell list
            cellList.cells[il][jl].indeces.append(i)

# function to calculate number of cells for numerical calculations using radius parameter
def calculateNumCellsRadius(x1, y1, x2, y2, rNeigh):

    # find out size of rectangle
    xl = x2 - x1
    yl = y2 - y1
    # calculate number of cells along every axes
    Ncx = trunc(xl / rNeigh)
    Ncy = trunc(yl / rNeigh)
    # return number of cells
    return Ncx, Ncy

# function to find indeces of the closest particles to the given cell
def findClosestParticlesCell(cellList, ic, jc):

    # create array of indeces
    array = []
    # find number of cells along axes
    Nx = cellList.Nx
    Ny = cellList.Ny

    # consider the given cell
    for i in range(len(cellList.cells[ic][jc].indeces)):
        array.append(cellList.cells[ic][jc].indeces[i])
    # consider the neighbour cell (ic - 1, jc)
    if ic >= 1:
        for i in range(len(cellList.cells[ic - 1][jc].indeces)):
            array.append(cellList.cells[ic - 1][jc].indeces[i])
    # consider the neighbour cell (ic, jc - 1)
    if jc >= 1:
        for i in range(len(cellList.cells[ic][jc - 1].indeces)):
            array.append(cellList.cells[ic][jc - 1].indeces[i])
    # consider the neighbour cell (ic + 1, jc)
    if ic <= Nx - 2:
        for i in range(len(cellList.cells[ic + 1][jc].indeces)):
            array.append(cellList.cells[ic + 1][jc].indeces[i])
    # consider the neighbour cell (ic, jc + 1)
    if jc <= Ny - 2:
        for i in range(len(cellList.cells[ic][jc + 1].indeces)):
            array.append(cellList.cells[ic][jc + 1].indeces[i])
    # consider the neighbour cell (ic - 1, jc - 1)
    if (ic >= 1) and (jc >= 1):
        for i in range(len(cellList.cells[ic - 1][jc - 1].indeces)):
            array.append(cellList.cells[ic - 1][jc - 1].indeces[i])
    # consider the neighbour cell (ic - 1, jc + 1)
    if (ic >= 1) and (jc <= Ny - 2):
        for i in range(len(cellList.cells[ic - 1][jc + 1].indeces)):
            array.append(cellList.cells[ic - 1][jc + 1].indeces[i])
    # consider the neighbour cell (ic + 1, jc - 1)
    if (ic <= Nx - 2) and (jc >= 1):
        for i in range(len(cellList.cells[ic + 1][jc - 1].indeces)):
            array.append(cellList.cells[ic + 1][jc - 1].indeces[i])
    # consider the neighbour cell (ic + 1, jc + 1)
    if (ic <= Nx - 2) and (jc <= Ny - 2):
        for i in range(len(cellList.cells[ic + 1][jc + 1].indeces)):
            array.append(cellList.cells[ic + 1][jc + 1].indeces[i])

    # return array of indeces
    return array

# function to calculate indeces for cell list
def calculateCellListIndex(cellList, xp, yp):

    # find out initial coordinates of cell structure
    x1 = cellList.x1
    y1 = cellList.y1
    # find out size of cell
    ax = cellList.ax
    by = cellList.by

    # determine indeces for particle
    ic = trunc((xp - x1) / ax)
    jc = trunc((yp - y1) / by)
    # return indeces
    return ic, jc
