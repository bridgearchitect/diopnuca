from scipy.spatial import Voronoi, voronoi_plot_2d
import matplotlib.pyplot as plt

# function to plot Voronoi diagram
def plotVoronoi(voronoi, points, regions, vertices, namePlot):

    # colorize
    for region in regions:
        polygon = vertices[region]
        plt.fill(*zip(*polygon), alpha=0.4)

    # build plot
    plt.plot(points[:,0], points[:,1], 'ko')
    plt.xlim(voronoi.min_bound[0] - 0.1, voronoi.max_bound[0] + 0.1)
    plt.ylim(voronoi.min_bound[1] - 0.1, voronoi.max_bound[1] + 0.1)

    # save figure
    plt.savefig(namePlot)
