# include modules
import numpy as np
from random import random
from general import *

# define particle structure
class Particle:
    # define constructor
    def __init__(self, x, y, f, ddX, ddY):
        # define particle fields
        self.x = x
        self.y = y
        self.f = f
        self.ddX = ddX
        self.ddY = ddY

# define array of particle structure
class ArrayParticle:
    # define constructor
    def __init__(self, Np):
        # define array of particles
        self.particles = [Particle(None, None, None, None, None) for i in range(Np)]

# function to create random particles in square
def createRandomParticleSquare(x1, y1, x2, y2, Np):

    # create array of particles
    arrayPart = ArrayParticle(Np)
    # fill array of random particles
    for i in range(Np):
        # generate random coordinates
        xr = x1 + (x2 - x1) * random()
        yr = y1 + (y2 - y1) * random()
        f = valueFunction(xr, yr)
        # fill one particle in array
        arrayPart.particles[i].x = xr
        arrayPart.particles[i].y = yr
        arrayPart.particles[i].f = f
    # return array of particles
    return arrayPart

# function to find average closest distance to neigbour particle
def findAverClosestDistance(arrayPart):

    # find number of particles
    Np = len(arrayPart.particles)
    # define average minimal distance
    averMinDist = 0.0

    # go through all particles
    for i in range(Np):
        # receive current particle
        curParticle = arrayPart.particles[i]
        # define minimal distance
        minDist = inf
        # go through all particles
        for j in range(Np):
            # check whether two particles are different
            if i == j:
                continue
            # receive neighbour particle
            neighParticle = arrayPart.particles[j]
            # calculate distance between two particles
            curDist = calcDistanceTwoParticles(curParticle, neighParticle)
            # find minimal distance for the given particle
            if curDist < minDist:
               minDist = curDist
        # find average minimal distance
        averMinDist += minDist

    # normalize average value of distance
    averMinDist = averMinDist / Np
    # return average minimal distance
    return averMinDist
