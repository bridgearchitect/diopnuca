# include modules
from geometry import *
from rand import *
from plot import *
from cellList import *
from particles import *
from general import *
from derivative import *
from write import *
from read import *

# define geometry constants
x1 = -1.0
y1 = -1.0
x2 = 1.0
y2 = 1.0
# define derivative constants
Scr = 0.05
# define string constants
derConstFileName = "derConst.txt"
plotName = "voronoi.png"
derErrorFileName = "derErrorFile.txt"

# read derivative constants
Np, rNeigh, coefEps = readDerivativeConstants(derConstFileName)
eps = coefEps * rNeigh

# create random particles in rectangle
arrayPart = createRandomParticleSquare(x1, y1, x2, y2, Np)
# find the avarage closest distance
averMinDist = findAverClosestDistance(arrayPart)
# calculate number of cells for numerical calculations using radius parameter
Ncy, Ncx = calculateNumCellsRadius(x1, y1, x2, y2, rNeigh)
# write critical geometry parameters
writeCriticalGeomParameters(rNeigh, eps, averMinDist)

# create cell list
cellList = CellList(x1, y1, x2, y2, Ncx, Ncy)
# fill cell list
fillList(cellList, arrayPart)

# calculate numerical derivative (laplacian)
calculateNumLaplacian(arrayPart, cellList, weightedFunctionXX, weightedFunctionYY, eps, Scr)
# calculate errors of calculated numerical derivatives
errorXX = calcErrorDerXX(arrayPart)
errorYY = calcErrorDerYY(arrayPart)

# write calculated numerical derivatives in file
writeNumericalDerivatives(arrayPart, derErrorFileName)
# write errors of calculated numerical derivatives in file
writeErrorNumericalDerivatives(errorXX, errorYY, derErrorFileName)
