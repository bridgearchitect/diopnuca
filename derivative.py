# include modules
from cellList import *
from particles import *
from general import *
from geometry import *
from plot import *

# function to convert indeces to particles
def convertIndecesToParticles(indeces, arrayPart):

    # find out number of indeces
    Ni = len(indeces)
    # create array of partilces
    particles = [None for i in range(Ni)]
    # go through all indeces
    for i in range(Ni):
        # receive current index
        index = indeces[i]
        # insert found particle in array
        particles[i] = arrayPart.particles[index]
    # return array of particles
    return particles

# function to convert particles into points
def convertParticlesToPoints(particles):

    # find out number of particles
    Np = len(particles)
    # create array of points
    points = np.zeros((Np,2))
    # go through all particles
    for i in range(Np):
        # copy coordinates
        points[i][0] = particles[i].x
        points[i][1] = particles[i].y
    # return array of points
    return points

# function to make Voronoi diagram using special Python library
def makeVoronoiDiagram(points, index):

    # calculate Voronoi diagram
    vorDiagram = doVoronoi(points)
    # reconstruct infinite Voronoi regions in a 2D diagram to finite regions
    regions, vertices = makeVoronoiFinitePolygons(vorDiagram)
    # debug code
    # if index == 0:
    #    plotVoronoi(vorDiagram, points, regions, vertices, "debug.png")
    # convert regions and vertices polygons
    polygons = convertRegionIntoPolygons(regions, vertices)
    # return create polygons
    return polygons

# function to calculate differential operators
def calculateDiffOperators(neighParticles, areas, weightedFunctionXX, weightedFunctionYY, eps, xp, yp, Scr):

    # find out number of neighbour particles
    Np = len(neighParticles)
    # initialization
    ddX = 0.0
    ddY = 0.0

    # go through all neighbour particles
    for i in range(Np):
        # find out coordinates of the current particle
        xc = neighParticles[i].x
        yc = neighParticles[i].y
        # find out value of particle
        fc = neighParticles[i].f
        # find area of polygon for this particle
        Sc = areas[i]
        # calculate value of differential operators
        if Sc <= Scr:
            ddX += fc * Sc * weightedFunctionXX(xp - xc, yp - yc, eps)
            ddY += fc * Sc * weightedFunctionYY(xp - xc, yp - yc, eps)

    # return value of differential operators
    return ddX, ddY

# function to calculate numerical laplacian using particle strength exchange
def calculateNumLaplacian(arrayPart, cellList, weightedFunctionXX, weightedFunctionYY, eps, Scr):

    # find out number of particles
    Np = len(arrayPart.particles)
    # go through all particles
    for i in range(Np):
        # receive current particle
        particle = arrayPart.particles[i]
        # find out coordinates of the current particle
        xp = particle.x
        yp = particle.y
        # define indeces for cell list
        ic, jc = calculateCellListIndex(cellList, xp, yp)
        # find all neighbour particles for the given cell
        neighIndexes = findClosestParticlesCell(cellList, ic, jc)
        # find neighbour particles using indeces
        neighParticles = convertIndecesToParticles(neighIndexes, arrayPart)
        # create points using array of neighbour particles
        neighPoints = convertParticlesToPoints(neighParticles)
        # make Voronoi using found neighbour points
        polygons = makeVoronoiDiagram(neighPoints, i)
        # calculate area for polygons
        areas = calculateAreaPolygons(polygons)
        # calculate value of differential operators
        ddX, ddY = calculateDiffOperators(neighParticles, areas, weightedFunctionXX, weightedFunctionYY, eps, xp, yp, Scr)
        # save differential operators in particle array
        arrayPart.particles[i].ddX = ddX
        arrayPart.particles[i].ddY = ddY
