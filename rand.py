# include modules
import numpy as np
from random import random

# function to create random points in rectangle
def createRectanlgeRandomPoints(N, x1, y1, x2, y2):

    # initialization
    points = np.zeros((N,2))
    # go through all points
    for i in range(N):
        # generate coordinates
        points[i][0] = x1 + (x2 - x1) * random()
        points[i][1] = y1 + (y2 - y1) * random()
    # return reduced array
    return points



