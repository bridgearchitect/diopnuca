# include modules
import numpy as np
import math
from scipy.spatial import Voronoi, voronoi_plot_2d

# function to do Voronoi triangulation
def doVoronoi(points):
    return Voronoi(points)

# function to reconstruct infinite Voronoi regions in a 2D diagram to finite regions
def makeVoronoiFinitePolygons(vor, radius=None):

    # handle critical situation
    if vor.points.shape[1] != 2:
        raise ValueError("Requires 2D input")

    # initialization
    newRegions = []
    newVertices = vor.vertices.tolist()
    center = vor.points.mean(axis=0)
    if radius is None:
        radius = vor.points.ptp().max()

    # construct a map containing all ridges for a given point
    allRidges = {}
    for (p1, p2), (v1, v2) in zip(vor.ridge_points, vor.ridge_vertices):
        allRidges.setdefault(p1, []).append((p2, v1, v2))
        allRidges.setdefault(p2, []).append((p1, v1, v2))

    # reconstruct infinite regions
    for p1, region in enumerate(vor.point_region):
        vertices = vor.regions[region]

        if all(v >= 0 for v in vertices):
            # finite region
            newRegions.append(vertices)
            continue

        # reconstruct a non-finite region
        ridges = allRidges[p1]
        newRegion = [v for v in vertices if v >= 0]

        # go through all ridge points
        for p2, v1, v2 in ridges:

            # handle all possible situations
            if v2 < 0:
                v1, v2 = v2, v1
            if v1 >= 0:
                # finite ridge: already in the region
                continue

            # compute the missing endpoint of an infinite ridge
            t = vor.points[p2] - vor.points[p1]
            t /= np.linalg.norm(t)
            n = np.array([-t[1], t[0]])
            midpoint = vor.points[[p1, p2]].mean(axis=0)
            direction = np.sign(np.dot(midpoint - center, n)) * n
            farPoint = vor.vertices[v2] + direction * radius
            # add new region and vertex
            newRegion.append(len(newVertices))
            newVertices.append(farPoint.tolist())

        # sort region counterclockwise
        vs = np.asarray([newVertices[v] for v in newRegion])
        c = vs.mean(axis=0)
        angles = np.arctan2(vs[:,1] - c[1], vs[:,0] - c[0])
        newRegion = np.array(newRegion)[np.argsort(angles)]
        # finish work
        newRegions.append(newRegion.tolist())

    # return answer
    return newRegions, np.asarray(newVertices)

# function to convert region into polygons
def convertRegionIntoPolygons(regions, vertices):

    # create polygons
    polygons = []
    for reg in regions:
        polygon = vertices[reg]
        polygons.append(polygon)
    # return polygons
    return polygons

# function to calculate absolute value of cross product
def calculateAbsCrossProduct(ax, bx, ay, by):
    return abs(ax * by - ay * bx)

# function to calculate area for polygons
def calculateAreaPolygons(polygons):

    # initialization
    nPol = len(polygons)
    areas = np.zeros(nPol)

    # go through all polygons
    i = 0
    for i in range(nPol):
        # define number of points in polygon
        nPoints = len(polygons[i])
        # calculate area of polygon
        j = 1
        while j < nPoints - 1:
            # receive coordinates of points
            x1 = polygons[i][0][0]
            y1 = polygons[i][0][1]
            x2 = polygons[i][j][0]
            y2 = polygons[i][j][1]
            x3 = polygons[i][j + 1][0]
            y3 = polygons[i][j + 1][1]
            # calculate vectors of triangle
            ax = x2 - x1
            ay = y2 - y1
            bx = x3 - x1
            by = y3 - y1
            # calculate area using cross product
            areas[i] += calculateAbsCrossProduct(ax, bx, ay, by) / 2.0
            # go to the next iteration
            j = j + 1

    # return array of areas
    return areas
