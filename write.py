# include modules
from general import *

# function to write critical geometry parameters
def writeCriticalGeomParameters(rNeigh, eps, averMinDist):

    # print critical geometry parameters in terminal
    print("Size of cell: ", rNeigh)
    print("Epsilon parameter for weighted function: ", eps)
    print("Average minimal distance between particles: ", averMinDist)

# function to write calculated numerical derivatives
def writeNumericalDerivatives(arrayPart, derErrorFileName):

    # find out number of points
    Np = len(arrayPart.particles)
    # create file
    derFile = open(derErrorFileName, "w")
    # write head row
    derFile.write("x y ddX ddY\n")
    # go through all particles
    for i in range(Np):
        # receive coordinates of the current particle
        xc = arrayPart.particles[i].x
        yc = arrayPart.particles[i].y
        # receive derivatives of the current particle
        ddX = arrayPart.particles[i].ddX
        ddY = arrayPart.particles[i].ddY
        # write information about current particle
        derFile.write(str(xc) + " " + str(yc) + " " + str(ddX) + " " + str(ddY) + "\n")
    # close file
    derFile.close()

# function to write errors of calculated numerical derivatives in file
def writeErrorNumericalDerivatives(errorXX, errorYY, derErrorFileName):

    # create file
    derFile = open(derErrorFileName, "a")
    # write information about numerical errors in file
    derFile.write("L2 error for XX-derivative: " + str(errorXX) + "\n")
    derFile.write("L2 error for YY-derivative: " + str(errorYY) + "\n")
    # close file
    derFile.close()
