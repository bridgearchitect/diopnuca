# include modules
from math import sqrt

# define infinite value for program project
inf = 100000000000.0
# define math constants
pi = 3.1415926535897932384626433
eu = 2.7182818284590452353602875

# define value function for calculation of derivative
def valueFunction(x, y):
    return x**2 + y**2

# define XX-derivative of value function for calculation of error
def valueFunctionXX(x, y):
    return 2.0

# define YY-derivative of value function for calculation of error
def valueFunctionYY(x, y):
    return 2.0

# define weighted function for calculation of derivative
def weightedFunction(x, y, eps):
    return eu**((-x**2 - y**2) / (4 * eps**2)) / (4.0 * eps**2 * pi)

# define XX-derivative of weighted function for calculation of derivative
def weightedFunctionXX(x, y, eps):
    return 1.0 * (eu**((-x**2 - y**2) / (4 * eps**2))) / (8.0 * eps**4 * pi) - \
           (x**2 * eu**((-x**2 - y**2) / (4 * eps**2))) / (16.0 * eps**6 * pi)

# define YY-derivative of weighted function for calculation of derivative
def weightedFunctionYY(x, y, eps):
    return 1.0 * (eu**((-x**2 - y**2) / (4 * eps**2))) / (8.0 * eps**4 * pi) - \
           (y**2 * eu**((-x**2 - y**2) / (4 * eps**2))) / (16.0 * eps**6 * pi)

# define XX-derivative of weighted function for calculation of derivative
# def weightedFunctionXX(x, y, eps):
#    return 1.0 * (eu**((-x**2 - y**2) / (4 * eps**2))) / (8.0 * eps**4 * pi) - \
#            (x**2 * eu**((-x**2 - y**2) / (4 * eps**2))) / (16.0 * eps**6 * pi)

# define YY-derivative of weighted function for calculation of derivative
# def weightedFunctionYY(x, y, eps):
#    return 1.0 * (eu**((-x**2 - y**2) / (4 * eps**2))) / (8.0 * eps**4 * pi) - \
#            (y**2 * eu**((-x**2 - y**2) / (4 * eps**2))) / (16.0 * eps**6 * pi)

# define function to calculate distance between two particles
def calcDistanceTwoParticles(particle1, particle2):
    # find out coordinates of the first particle
    x1 = particle1.x
    y1 = particle1.y
    # find out coordinates of the second particle
    x2 = particle2.x
    y2 = particle2.y
    # calculate and return distance
    return sqrt((x2 - x1)**2 + (y2 - y1)**2)

# define element in array using the specified value
def deleteElementInArray(array, val):
    # remove extra element
    array.remove(val)

# define function to calculate error for XX-derivative of value function
def calcErrorDerXX(arrayPart):

    # initialization
    err = 0.0
    # find out number of particles
    Np = len(arrayPart.particles)
    # go through all particles
    for i in range(Np):
        # find out coordinates of particle
        xc = arrayPart.particles[i].x
        yc = arrayPart.particles[i].y
        # find out value of numerical and theoretical derivatives
        ddXnum = arrayPart.particles[i].ddX
        ddXthe = valueFunctionXX(xc, yc)
        # add to error square of difference
        err += (ddXnum - ddXthe)**2
    # normalize and calculate square root of calculated sum
    err = sqrt(err / Np)
    # return error value
    return err

# define function to calculate error for YY-derivative of value function
def calcErrorDerYY(arrayPart):

    # initialization
    err = 0.0
    # find out number of particles
    Np = len(arrayPart.particles)
    # go through all particles
    for i in range(Np):
        # find out coordinates of particle
        xc = arrayPart.particles[i].x
        yc = arrayPart.particles[i].y
        # find out value of numerical and theoretical derivatives
        ddYnum = arrayPart.particles[i].ddY
        ddYthe = valueFunctionYY(xc, yc)
        # add to error square of difference
        err += (ddYnum - ddYthe)**2
    # normalize and calculate square root of calculated sum
    err = sqrt(err / Np)
    # return error value
    return err
